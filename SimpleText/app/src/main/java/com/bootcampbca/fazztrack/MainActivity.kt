package com.bootcampbca.fazztrack

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.bootcampbca.fazztrack.ui.theme.FazztrackTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
                content = {
                    // Here, SimpleText is a Composable function which is going to describe the contents of
                    // this activity that will be rendered on the screen.
                    SimpleText("Saya Belajar Jetpack Compose UI")
                }
            )
        }
    }
}

@Composable
fun SimpleText(displayText: String) {

    Text(text = displayText)
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    FazztrackTheme {
        SimpleText("Android")
    }
}